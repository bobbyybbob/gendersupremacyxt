﻿// Assembly-CSharp, Version=1.4.8706.7168, Culture=neutral, PublicKeyToken=null
// RimWorld.ThoughtWorker_Precept_ColonyXenotypeMakeup
using System.Collections.Generic;
using RimWorld;
using Verse;
    public class ThoughtWorker_Precept_ColonyGenderMakeupF : ThoughtWorker_Precept
    {
        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
        if (!ModsConfig.BiotechActive || !ModsConfig.IdeologyActive || p.Faction == null)
        {
            return ThoughtState.Inactive;
        }
        if (p.gender.ToString() != "Female")
        {
            return ThoughtState.Inactive;
        }
        List<Pawn> list = p.MapHeld.mapPawns.SpawnedPawnsInFaction(p.Faction);
            int num = 0;
            int num2 = 0;
            foreach (Pawn item in list)
            {            
            if (item.gender.ToString() == "Female" && !item.IsSlave && !item.IsPrisoner && item.IsColonist)
                {
                    num++;
                    
                } else if (!item.IsSlave && !item.IsPrisoner && item.IsColonist)
            {
                num2++;
            }
            }
        if (num2 == 0)
            {
                return ThoughtState.ActiveAtStage(0);
            }
            float num3 = (float)num2 / (float)num;
            if (num3 < 0.33f)
            {
                return ThoughtState.ActiveAtStage(1);
            }
            if (num3 < 0.66f)
            {
                return ThoughtState.ActiveAtStage(2);
            }
            return ThoughtState.ActiveAtStage(3);
        }

    }

public class ThoughtWorker_Precept_ColonyGenderMakeupStrictF : ThoughtWorker_Precept
{
    protected override ThoughtState ShouldHaveThought(Pawn p)
    {
        if (!ModsConfig.BiotechActive || !ModsConfig.IdeologyActive || p.Faction == null)
        {
            return ThoughtState.Inactive;
        }
        if (p.gender.ToString() != "Female")
        {
            return ThoughtState.Inactive;
        }
        List<Pawn> list = p.MapHeld.mapPawns.SpawnedPawnsInFaction(p.Faction);
        foreach (Pawn item in list)
        {
            if (item.gender.ToString() != "Female" && !item.IsSlave && !item.IsPrisoner && item.IsColonist)
            {

                return ThoughtState.ActiveAtStage(1);
            }
        }
        return ThoughtState.ActiveAtStage(0);
    }

}


public class ThoughtWorker_Precept_ColonyGenderMakeupM : ThoughtWorker_Precept
    {
        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
            if (!ModsConfig.BiotechActive || !ModsConfig.IdeologyActive || p.Faction == null)
            {
                return ThoughtState.Inactive;
            }
            if (p.gender.ToString() != "Male")
            {
                return ThoughtState.Inactive;
            }
            List<Pawn> list = p.MapHeld.mapPawns.SpawnedPawnsInFaction(p.Faction);
            int num = 0;
            int num2 = 0;
            foreach (Pawn item in list)
            {
                if (item.gender.ToString() == "Male" && !item.IsSlave && !item.IsPrisoner && item.IsColonist)
                {
                    num++;

                }
                else if (!item.IsSlave && !item.IsPrisoner && item.IsColonist)
                {
                    num2++;
                }
            }
            if (num2 == 0)
            {
                return ThoughtState.ActiveAtStage(0);
            }
            float num3 = (float)num2 / (float)num;
            if (num3 < 0.33f)
            {
                return ThoughtState.ActiveAtStage(1);
            }
            if (num3 < 0.66f)
            {
                return ThoughtState.ActiveAtStage(2);
            }
            return ThoughtState.ActiveAtStage(3);
        }

    }

    public class ThoughtWorker_Precept_ColonyGenderMakeupStrictM : ThoughtWorker_Precept
    {
        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
            if (!ModsConfig.BiotechActive || !ModsConfig.IdeologyActive || p.Faction == null)
            {
                return ThoughtState.Inactive;
            }
            if (p.gender.ToString() != "Male")
            {
                return ThoughtState.Inactive;
            }
            List<Pawn> list = p.MapHeld.mapPawns.SpawnedPawnsInFaction(p.Faction);
            foreach (Pawn item in list)
            {
                if (item.gender.ToString() != "Male" && !item.IsSlave && !item.IsPrisoner && item.IsColonist)
                {
                return ThoughtState.ActiveAtStage(1);

                }
            }
            return ThoughtState.ActiveAtStage(0);
        }
    }

public class ThoughtWorker_Precept_SelfDislikedGenderF : ThoughtWorker_Precept
    {
        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
        return p.gender.ToString() != "Female";
        }
    }

    public class ThoughtWorker_Precept_SelfDislikedGenderM : ThoughtWorker_Precept
    {
        protected override ThoughtState ShouldHaveThought(Pawn p)
        {
        return p.gender.ToString() == "Female";
        }
    }

    public class ThoughtWorker_Precept_PreferredGender_SocialF : ThoughtWorker_Precept_Social
    {
        protected override ThoughtState ShouldHaveThought(Pawn p, Pawn otherPawn)
        {
            if (otherPawn.gender.ToString() == "Female")
            {
                return ThoughtState.ActiveAtStage(0);
            }
            return ThoughtState.ActiveAtStage(1);
        }
    }

    public class ThoughtWorker_Precept_PreferredGender_SocialM : ThoughtWorker_Precept_Social
    {
        protected override ThoughtState ShouldHaveThought(Pawn p, Pawn otherPawn)
        {
            if (otherPawn.gender.ToString() != "Female")
            {
                return ThoughtState.ActiveAtStage(0);
            }
            return ThoughtState.ActiveAtStage(1);
        }
    }

