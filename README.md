Small mod that adds a new precept to the gender supremacy memes for your colony full of amazonian women or naked cowboys at ram ranch. Requires Ideology DLC. Designed for play with a RJW mod list, but not strictly required. Consider using a mod that allows changing genders like RJW, Licentia Labs or Surgical Body Shaping(sfw).

# Currently Implemented: #
* 2 new precepts, Female supremacy and Male supremacy

## Supremacy ##
* Each gender can have supremacy measured as required, essential, or important
* Mood bonuses and penalties based on the gender composition of your colony
* Relationship bonuses and penalties based on gender
* Mood penalties if own gender is opposite of ideologies'

Gender is determined by whatever vanilla Rimworld says, not biological RJW parts so your colony of hot futanari dommy mommies or femboy traps won't complain.
